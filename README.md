# DoT-mitm (DNS-over-TLS Man-in-the-Middle)
## But why?
This project was created because I had implemented DNS-over-TLS to CloudFlare (1.1.1.1, 1.0.0.1) and quickly realized any network detection tools in the path would be unable to detect or log DNS queries and answers. You may be asking, well that's the point, right? Partly. DNS-over-TLS prevents prying eyes to either view or manipulate DNS queries and answers, but doesn't exactly help network defenders. Just like any other network encryption, the only real way to see this data is either on the end host (complex and you'd need to deploy a solution to EVERYTHING... if they all even support DNS sniffing) or break it in transit. I chose to do the latter

## Demo Architecture
In short, this is a TLS break-and-inspect setup with a downstream server (facing the client) and an upstream server (forwarding to CloudFlare's DoT servers). The traffic between the two are plain text (and never leave the same physical box because... Docker), so when the upstream server receives the traffic, it can be analyzed. This is where <a href="https://www.elastic.co/products/beats/packetbeat">packetbeat</a> comes in. Packetbeat can analyze and log a TON of different network protocols, but I'm simply sniffing for any DNS traffic and writing to a log file. To implement in the "real world", packetbeat is flexible enought that you could switch to either writing the log data to local syslog (to later be forwarded to a SIEM) or send directly to the SIEM over TLS (ironic... isn't it).

TODO: Add diagrams
## Demo How-To Guide
1. There are a couple of pre-requisites to make this work:
- A Debian operating system within a virtual machine (or another Debian-based Docker container)
- Docker Community Edition (with docker-compose)
2. On the Debian machine, you'll need to install stubby and place the stubby.yml file at /etc/stubby/stubby.yml. Update the sixth line from the bottom to be the IP address of your host machine's network interface.
```
sudo apt-get install stubby
...copy stubby.yml to /etc/stubby/stubby.yml...
systemctl enable stubby
systemctl start stubby
```
3. Copy setdns.sh to Debian machine and run the following command (or you can just set your DNS to 127.0.0.1):
```
sudo /path/to/setdns.sh local
```
- Note: You can set the DNS to Google's DNS servers (bypassing the DoT downstream server) by running this command:
```
sudo /path/to/setdns.sh google
```
4. Start the Docker containers on the host system by running the demo.sh file
```
cd /path/to/dot-mitm/
./demo.sh
```
5. If all was successful, you should now be in the dot-mitm_dot-upstream_1 container
6. Go back to the Debian machine and start doing DNS things (nslookup, web browsing, pinging a FQDN, etc). For example:
```
nslookup www.ryanic.com
```

<img src="https://gitlab.com/ryanic/dot-mitm/raw/master/images/nslookup.png" width=50%/>

7. Back at the dot-mitm_dot-upstream_1 container, issue the following command to see the dns requests
```
/root/checkdns.sh
```
- Note: To see more DNS detail, run this command:
```
cat /var/log/dns.log | jq .
```
8. When finished, exit out of the dot-mitm_dot-upstream_1 container
```
exit
```
9. Please note that, for your Debian machine to function without these containers running, you'll need to reset your DNS settings (or use Google as demonstrated below):
```
sudo /path/to/setdns.sh google
```
## Proof that this is actually breaking the DNS-over-TLS and gathering data
Here's what the network traffic headed to dot-mitm_dot-downstream_1 looks like:

<img src="https://gitlab.com/ryanic/dot-mitm/raw/master/images/to-downstream.png" width=50%/>

Here's the traffic departing dot-mitm_dot-upstream_1 to CloudFlare's DoT-enabled DNS:

<img src="https://gitlab.com/ryanic/dot-mitm/raw/master/images/from-upstream.png" width=50%/>

And, finally, here's the log entry:

<img src="https://gitlab.com/ryanic/dot-mitm/raw/master/images/dnslog.png" width=50%/>