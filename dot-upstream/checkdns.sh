#!/bin/bash

cat /var/log/dns.log | jq '. | {client: .client_ip, query: .query, answer: .dns.answers[].data}' 2>/dev/null
