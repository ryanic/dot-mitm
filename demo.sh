#!/bin/bash
echo -e "\033[95mStarting Docker containers...\033[0m"
docker-compose up --build -d
clear
echo -e "\033[95m############ Welcome to dot-mitm_dot-upstream_1! #############"
echo -e "# Use nslookup on the client and then run: /root/checkdns.sh #"
echo -e "##############################################################\033[0m"
docker exec -it dot-mitm_dot-upstream_1 /bin/bash
echo -e "\033[95mStopping Docker containers...\033[0m"
docker-compose down
