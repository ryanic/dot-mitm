#!/bin/bash

if [ $# -ne 1 ]; then
  echo "NEED TO ADD 'local' or 'google'!"
  exit
fi

if [ $1 == "local" ]; then
  nmcli con mod "Wired connection 1" ipv4.dns "127.0.0.1"
fi

if [ $1 == "google" ]; then
  nmcli con mod "Wired connection 1" ipv4.dns "8.8.8.8, 8.8.9.9"
fi

nmcli dev disconnect eth0 > /dev/null
nmcli dev connect eth0 > /dev/null
/bin/cat /etc/resolv.conf | grep nameserver
